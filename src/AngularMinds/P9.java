package AngularMinds;

public class P9 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int space = 1;
        int ch = 5;
        for (int i = 0; i < line; i++) {
            int ch1 = ch;
            for (int j = 1; j < space; j++) {
                System.out.print(ch1++);
            }
            for (int k = 0; k < star; k++) {
                System.out.print("5");
            }
            ch--;
            star--;
            space++;
            System.out.println();
        }
    }
}
