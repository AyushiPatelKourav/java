package AngularMinds;

public class P14 {
    public static void main(String[] args) {
        int line =4;
        int star=1;
        int space=line-1;
        int ch=1;
        for(int i=0;i<line;i++){
            for(int j=0;j<space;j++){
                System.out.print(" ");
            }
            for(int k=0;k<star;k++){
                System.out.print(" "+ch++);
            }
            System.out.println();
            star++;
            space--;
        }
    }
}
