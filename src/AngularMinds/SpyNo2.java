package AngularMinds;

public class SpyNo2 {
    public static void main(String[] args) {

        for(int i=10;i<=100000;i++){
            int a=i;
            int temp=a;
            int sum=0;
            int multiply=1;
            while(temp!=0){
                int r=temp%10;
                sum+=r;
                multiply*=r;
                temp/=10;
            }
            if(sum==multiply){
                System.out.println(i);
            }

        }
    }
}
