package Inheritance;

public class MainApp3 {
    public static void main(String[] args) {
        PermanentEmployee p1=new PermanentEmployee();
        p1.getInfo(1,40000);
        p1.permanentInfo("ANALYST");
        ContractEmployee c1=new ContractEmployee();
        c1.getInfo(02,50000);
        c1.contractInfo(3);
    }
}
