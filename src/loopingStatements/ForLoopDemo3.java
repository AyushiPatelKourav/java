package loopingStatements;
import java.util.Scanner;
public class ForLoopDemo3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        int a=sc1.nextInt();
        int b=sc1.nextInt();
        for(int c=a;c<=b;c++)
        {
            if(c%2==0)
            {
                System.out.println(c*c);
            }
        }
    }
}
