package loopingStatements;
import java.util.Scanner;
public class ForLoopDemo5 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        int a=sc1.nextInt();
        int b=sc1.nextInt();
        int sum=0;
        for(int c=a;c<=b;c++)
        {
            if(c%2==0)
            {
               sum+=c;
            }
        }
        System.out.println(sum);
    }
}
