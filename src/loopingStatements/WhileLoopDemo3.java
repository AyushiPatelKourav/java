package loopingStatements;
import java.util.Scanner;
public class WhileLoopDemo3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        boolean status=true;
        while(status)
        {
            System.out.println("1: ADDITION");
            System.out.println("2: SUBSTRACTION");
            System.out.println("3: EXIT");
            int choice =sc1.nextInt();
            if(choice==1)
            {
                System.out.println("ENTER FIRST NO.");
                int n1 =sc1.nextInt();
                System.out.println("ENTER SECOND NO.");
                int n2=sc1.nextInt();
                System.out.println(n1+n2);
            }
            else if(choice==2)
            {
                System.out.println("ENTER FIRST NO.");
                int n1=sc1.nextInt();
                System.out.println("ENTER SECOND NO.");
                int n2=sc1.nextInt();
                System.out.println(n1-n2);
            }
            else
            {
                status=false;
            }

        }
    }
}
