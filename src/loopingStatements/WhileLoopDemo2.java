package loopingStatements;
import java.util.Scanner;
public class WhileLoopDemo2 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        int n;
        int sum = 0;
        while (sum <= 50) {
            System.out.println("ENTER NO.");
            n = sc1.nextInt();
            sum += n;
            System.out.println(sum);
        }
    }
}