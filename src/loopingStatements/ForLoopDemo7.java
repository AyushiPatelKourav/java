package loopingStatements;
import java.util.Scanner;
public class ForLoopDemo7 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        int line=sc1.nextInt();
        int star =1;
        for(int row=1;row<=line;row++)
        {
            for(int col=1;col<=star;col++)
            {
                System.out.print("*" + "\t");
            }
            System.out.println();
            star++;
        }

    }
}
