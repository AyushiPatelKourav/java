package String;

public class StringDemo {
    public static void main(String[] args) {
        String s="SOFTWARE DEVELOPER" ;
        System.out.println(s.length());
        System.out.println(s.charAt(12));
        System.out.println(s.indexOf('W'));
        System.out.println(s.lastIndexOf('E'));
        System.out.println(s.contains("EVE"));
        System.out.println(s.startsWith("SOFT"));
        System.out.println(s.endsWith("PER"));
        System.out.println(s.substring(9));
        System.out.println(s.substring(0,8));
        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());
    }
}
