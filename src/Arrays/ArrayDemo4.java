package Arrays;
import java.util.Scanner;
public class ArrayDemo4 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Total No of Courses");
        int size =sc1.nextInt();
        double [] marks=new double[size];
        double total=0;
        System.out.println("Enter Marks");
        for(int i=0;i<size;i++)
        {
            marks [i] =sc1.nextDouble();
            total+=marks[i];
        }
        System.out.println("TOTAL MARKS = "+total);
        System.out.println("PERCENTAGE = "+total/size);
    }
}
