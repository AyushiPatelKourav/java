package Arrays;

public class StoreManager {
    static String [] product={"TV","PROJECTOR","MOBILE"};
    static double [] price={12000.34,15000.26,20000.00};
    static int [] stocks={20,30,40};

    void calculateBill(int choice , int qty)
    {
        boolean found =false;
        for(int i=0;i<product.length;i++)
        {
            if(choice==i && qty<=stocks[i])
            {
                double total=price[i]*qty;
                stocks[i]-=qty;
                System.out.println("TOTAL COST ="+total);
                found=true;
            }
        }
        if(!found)
        {
            System.out.println("PRODUCT NOT FOUND OR ELSE OUT OF STOCK");
        }
    }

}
