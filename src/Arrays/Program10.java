package Arrays;

public class Program10 {
    public static void main(String[] args) {
        char[] info = takeArray();
        for (int i = 0; i < info.length; i++) {
            System.out.println(info[i]);
        }
    }

    static char[] takeArray() {
        char[] data = new char[3];
        data[0] = 'A';
        data[1] = 'B';
        data[2] = 'C';
        return data;
    }
}