package Arrays;
import java.util.Scanner;
public class ArrayDemo11 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        BillCalculator b1=new BillCalculator();
        System.out.println("ENTER TOTAL NO OF BILLS");
        int count=sc1.nextInt();
        System.out.println("ENTER BILL AMOUNT");
        double [] amt=new double[count];
        for(int i=0;i<count;i++)
        {
            amt [i]=sc1.nextDouble();
        }
        b1.calculateBill(amt);
    }
}
