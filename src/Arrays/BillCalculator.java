package Arrays;

public class BillCalculator {
    public double[] gstCalculation(double[] amt) {
        double[] gstAmt = new double[amt.length];
        for (int i = 0; i < amt.length; i++) {
            if (amt[i] < 500) {
                gstAmt[i] = amt[i] * 0.05;
            } else {
                gstAmt[i] = amt[i] * 0.1;
            }
        }
        return gstAmt;
    }
        public void calculateBill(double []amt)
        {
            double [] gstValue=gstCalculation(amt);
            double [] totalAmt=new double[amt.length];
            for(int i=0;i<amt.length;i++)
            {
                totalAmt[i]=amt[i] + gstValue[i];
            }
            double totalBillAmt=0;
            double totalGstAmt=0;
            double totalFinalAmt=0;

            for(int i=0;i<amt.length;i++)
            {
                totalBillAmt+=amt[i];
                totalGstAmt+=gstValue[i];
                totalFinalAmt+=totalAmt[i];
            }

            System.out.println("BIL.AMT \t GST.AMT \t TOTAL");
            System.out.println("=========================================");
            for(int i=0;i<amt.length;i++)
            {
                System.out.println(amt[i]+"    \t"+ gstValue[i]+"    \t"+totalAmt[i]+"    \t");
            }
            System.out.println("==========================================");
            System.out.println(totalBillAmt+" \t"+totalGstAmt+" \t"+totalFinalAmt+" \t");
        }
    }
