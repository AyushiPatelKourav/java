package Arrays;
import java.util.Scanner;
public class ArrayDemo3 {
    public static void main(String[] args) {
        Scanner sc1 =new Scanner(System.in);
        System.out.println("Enter Total No of Courses");
        int count =sc1.nextInt();
        String [] courses = new String[count];
        System.out.println("Enter Courses Name");
        for(int i=0;i<count;i++)
        {
           courses[i]=sc1.next();
        }
        System.out.println("Selected Courses");
        for(int i=0;i<count;i++) {
            System.out.println(courses[i]);
        }
    }
}
