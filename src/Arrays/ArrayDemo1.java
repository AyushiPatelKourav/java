package Arrays;

public class ArrayDemo1 {
    public static void main(String[] args) {
        int [] data;             //declaration
        data =new int[5];            //size allocation
        data[0]=100;
        data[1]=200;                   // initialisation
        data[2]=300;
        data[3]=400;
        data[4]=500;
        for(int a=0;a<5;a++)
        {
            System.out.println(data[a]);

        }

    }
}
