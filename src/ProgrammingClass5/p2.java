package ProgrammingClass5;

public class p2 {
    public static void main(String[] args) {
        int line=4;
        int star=1;
        int ch=0;
        int space=3;
        for(int i=0;i<line;i++){
            for(int j=0;j<space;j++){
                System.out.print(" ");
            }
            for(int k=0;k<star;k++){
                if(i>k)
                {
                    System.out.print(ch--);
                }
                else{
                    System.out.print(ch++);
                }
            }
            System.out.println();
            space--;
            star+=2;
        }
    }
}
