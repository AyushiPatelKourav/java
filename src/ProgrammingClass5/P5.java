package ProgrammingClass5;

public class P5 {
    public static void main(String[] args) {
        int line = 9;
        int star = 5;
        int space = 0;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < star; k++) {
                System.out.print(" *");
            }
            if (i <= 3) {
                space++;
                star--;
            } else {
                space--;
                star++;
            }
            System.out.println();
        }
    }
}