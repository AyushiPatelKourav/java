package abstraction;

public class SavingAccount implements Account{
double accountBalance;
public  SavingAccount(double accountBalance){
this.accountBalance=accountBalance;
    System.out.println("Saving Account Created");
    }
    @Override
    public void deposite(double amt) {
        accountBalance+=amt;
        System.out.println(amt+" Rs Credited to your account");
    }

    @Override
    public void withdraw(double amt) {
if(amt<=accountBalance){
    accountBalance-=amt;
    System.out.println(amt+" Rs Debited from your Account");
}else {
    System.out.println("Insufficiet Accout Balnce");
}
    }
@Override
    public void checkBalance() {
        System.out.println("Active Balnce " + accountBalance); }
}
