package abstraction;

import java.util.Scanner;

// utilization layar
public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Account Type");
        System.out.println(" 1:SAVING \n 2: LOAN");
        int acctype=sc1.nextInt();
        System.out.println("Enter Amount Openning Balance");
        double balance=sc1.nextDouble();


        AccountFactory factory=new AccountFactory();
        Account accRef=factory.createAccount(acctype , balance);
        boolean status =true;
        while (status){
            System.out.println(" Select Mode Of Transaction");
            System.out.println(" 1: DEPOSIT \n 2: WITHDRAW \n 3: CHECK BALANCE \n 4: EXIT");
            int choice = sc1.nextInt();
            if(choice==1){
                System.out.println("Enter Amount");
                double amt= sc1.nextDouble();
                accRef.deposite(amt);
                } else if (choice==2) {
                System.out.println("Enter Amount");
                double amt= sc1.nextDouble();
                accRef.withdraw(amt);

            } else if (choice==3) {
                accRef.checkBalance();

            }
            else {
                status=false;
        }
        }
    }
}
