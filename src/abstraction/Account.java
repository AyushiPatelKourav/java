package abstraction;

public interface Account {
    void deposite(double amt);
    void withdraw(double amt);
    void checkBalance();

}
