package abstraction;

public class LoanAccount implements Account {
private double loanAmount;
public LoanAccount(double loanAmount){
    this.loanAmount=loanAmount;
    System.out.println("Loan Account Created");
}

    @Override
    public void deposite(double amt) {
        loanAmount-=amt;
        System.out.println((amt+" Rs Debited from Loan Account"));
    }

    @Override
    public void withdraw(double amt) {
loanAmount+=amt;
        System.out.println(amt+" Rs Creadited to your Account");
    }

    @Override
    public void checkBalance() {
        System.out.println("ACTIVE LOAN AMOUNT "+loanAmount);
    }
}
