package Overloading;
import java.util.Scanner;
public class MainApp1 {
    public static void main(String[] args) {
        Student s1=new Student();
        Scanner sc1 =new Scanner (System.in);
        System.out.println("SELECT SEARCH CRITERIA");
        System.out.println("1: SEARCH BY NAME \n 2: SEARCH BY CONTACT");
        int choice=sc1.nextInt();
        if(choice==1){
            System.out.println("ENTER NAME");
            String name=sc1.next();
            s1.search(name);
        }
        else if(choice==2){
            System.out.println("ENTER CONTACT NO ");
            int contact=sc1.nextInt();
            s1.search(contact);
        }
        else {
            System.out.println("INVALID CHOICE ");
        }
    }
}
