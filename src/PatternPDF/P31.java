package PatternPDF;

public class P31 {
    public static void main(String[] args) {
        int line=6;
        int star=1;
        int space=line-1;

        for(int i=0;i<line;i++){
            char ch='A';
            for(int j=0;j<space;j++){
                System.out.print(" ");
            }
            for(int k=0;k<star;k++){
                System.out.print(" "+ch++);
            }
            System.out.println();
            star++;
            space--;
        }
    }
}
