package PatternPDF;

public class P20 {
    public static void main(String[] args) {
        int line = 9;
        int star = 10;
        int space = 0;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star / 2; j++) {
                System.out.print("*");
            }

            for (int j = 0; j < space; j++) {
                System.out.print(" ");

            }

            for (int k = star/2; k < star; k++) {
                System.out.print("*");
            }
                if (i <= 4) {
                    star--;
                    space += 2;
                } if(i>4){
                    star++;
                    space -= 2;
                }

            System.out.println();
        }
    }
}
