package PatternPDF;

public class P42 {
    public static void main(String[] args) {
        int line =10;
        int star=5;
        int space=0;
        int ch=1;
        for(int i=0;i<line;i++){
          for(int j=0;j<space;j++){
              System.out.print(" ");
          }
          int ch1=ch;
          for(int k=0;k<star;k++){
              System.out.print(" "+ch1++);
          }
            System.out.println();
          if(i<=4){
              space++;
              star--;
              ch++;
          }
          if(i>=4){
              space--;
              star++;
              ch--;
          }

        }
    }
}
