package PatternPDF;

public class P10 {
    public static void main(String[] args) {
        int line=5;
        int star=1;
        int space=line-1;
        for(int i=0;i<line;i++){
            for(int j=0;j<space;j++){
                System.out.print(" ");
            }
            for(int k=0;k<star;k++){
                System.out.print("*");
            }
            star+=2;
            space--;

                System.out.println();
            }
        }
    }

